-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 16, 2022 lúc 10:17 AM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_banhang`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `admin_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `email`, `password`, `admin_name`) VALUES
(1, 'admin@gmail.com', 'admin', 'VanThai');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`) VALUES
(1, 'Laptop'),
(2, 'Tủ lạnh'),
(3, 'Máy giặt'),
(4, 'Điện thoại'),
(5, 'Tivi');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_donhang`
--

CREATE TABLE `tbl_donhang` (
  `donhang_id` int(11) NOT NULL,
  `sanpham_id` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  `mahang` varchar(100) NOT NULL,
  `khachhang_id` int(11) NOT NULL,
  `ngaythang` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tinhtrang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_donhang`
--

INSERT INTO `tbl_donhang` (`donhang_id`, `sanpham_id`, `soluong`, `mahang`, `khachhang_id`, `ngaythang`, `tinhtrang`) VALUES
(51, 40, 1, '5468', 49, '2022-05-11 15:56:26', 0),
(52, 4, 1, '5468', 49, '2022-05-11 15:56:26', 0),
(53, 6, 1, '5468', 49, '2022-05-11 15:56:26', 0),
(54, 40, 1, '5702', 50, '2022-05-11 15:57:36', 0),
(55, 2, 1, '5843', 26, '2022-05-13 15:42:07', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_giaodich`
--

CREATE TABLE `tbl_giaodich` (
  `giaodich_id` int(11) NOT NULL,
  `sanpham_id` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  `magiaodich` varchar(100) NOT NULL,
  `ngaythang` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `khachhang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_giaodich`
--

INSERT INTO `tbl_giaodich` (`giaodich_id`, `sanpham_id`, `soluong`, `magiaodich`, `ngaythang`, `khachhang_id`) VALUES
(13, 1, 1, '8382', '2022-04-23 15:20:23', 37),
(14, 1, 1, '8527', '2022-04-23 15:22:52', 38),
(15, 2, 1, '8103', '2022-04-23 15:23:22', 39),
(16, 2, 1, '3483', '2022-04-23 15:25:43', 40),
(17, 1, 1, '8494', '2022-04-23 15:26:05', 41),
(18, 1, 1, '3990', '2022-04-23 15:26:43', 42),
(19, 5, 1, '6392', '2022-04-23 15:27:22', 43),
(20, 4, 1, '878', '2022-04-23 15:38:01', 45),
(21, 5, 1, '878', '2022-04-23 15:38:01', 45),
(22, 5, 1, '8615', '2022-04-26 09:00:35', 46),
(23, 1, 1, '8615', '2022-04-26 09:00:35', 46),
(24, 34, 1, '587', '2022-04-26 10:04:29', 46),
(25, 6, 1, '587', '2022-04-26 10:04:29', 46),
(26, 1, 1, '9313', '2022-04-26 10:05:58', 46),
(27, 2, 1, '9313', '2022-04-26 10:05:58', 46),
(28, 1, 1, '5692', '2022-04-30 09:05:46', 26),
(29, 40, 1, '5468', '2022-05-11 15:56:26', 49),
(30, 4, 1, '5468', '2022-05-11 15:56:26', 49),
(31, 6, 1, '5468', '2022-05-11 15:56:26', 49),
(32, 40, 1, '5702', '2022-05-11 15:57:36', 50),
(33, 2, 1, '5843', '2022-05-13 15:42:07', 26);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_giohang`
--

CREATE TABLE `tbl_giohang` (
  `giohang_id` int(11) NOT NULL,
  `tensanpham` varchar(100) NOT NULL,
  `sanpham_id` int(11) NOT NULL,
  `giasanpham` varchar(50) NOT NULL,
  `hinhanh` varchar(50) NOT NULL,
  `soluong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_khachhang`
--

CREATE TABLE `tbl_khachhang` (
  `khachhang_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_khachhang`
--

INSERT INTO `tbl_khachhang` (`khachhang_id`, `name`, `phone`, `address`, `email`, `password`) VALUES
(26, 'Hoàng Đình Đăng', '6868', 'Hà nội', 'dang@gmail.com', '123'),
(42, 'Tran Van A', '135', 'Thái Bình', 'a@gmail.com', 'abc'),
(43, 'Tran Van Binh', ' 123', 'Bắc Giang', 'binh@gmail.com', 'abc'),
(44, 'nguyen van c', '234', 'Hải Phòng', 'c@gmail.com', '567'),
(45, 'Phụng Nguyễn', '234', 'Hà nội', 'phung@gmail.com', 'abc'),
(47, 'Do Van Tung', '234', 'Hải Phòng', 't@gmail.com', 'abbbbbb'),
(48, 'Hoang Van Tuan', '234', 'Hải Phòng', 'c@gmail.com', 'gh'),
(49, 'Thu', '999', 'Quang Ninh', 'thu@gmail.com', 'abc'),
(50, 'Tran Van B', '234', 'Hải Phòng', 'abc@gmail.com', '123');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_sanpham`
--

CREATE TABLE `tbl_sanpham` (
  `sanpham_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sanpham_name` varchar(255) NOT NULL,
  `sanpham_chitiet` text NOT NULL,
  `sanpham_mota` text NOT NULL,
  `sanpham_gia` varchar(100) NOT NULL,
  `sanpham_giakhuyenmai` varchar(100) NOT NULL,
  `sanpham_active` int(11) NOT NULL,
  `sanpham_hot` int(11) NOT NULL,
  `sanpham_soluong` int(11) NOT NULL,
  `sanpham_image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_sanpham`
--

INSERT INTO `tbl_sanpham` (`sanpham_id`, `category_id`, `sanpham_name`, `sanpham_chitiet`, `sanpham_mota`, `sanpham_gia`, `sanpham_giakhuyenmai`, `sanpham_active`, `sanpham_hot`, `sanpham_soluong`, `sanpham_image`) VALUES
(1, 4, 'Samsung galaxy A10', '', '', '4000000', '3800000', 1, 0, 10, 'm2.jpg'),
(2, 4, 'Iphone 7s', '', '', '5000000', '4800000', 1, 0, 10, 'm3.jpg'),
(3, 3, 'Máy giặt Toshiba', '', '', '10000000', '9000000', 1, 0, 10, 'toshi.jpg'),
(4, 3, 'Máy giặt Sony', '', '', '13000000', '9500000', 1, 0, 10, 'm8.jpg'),
(5, 5, 'Tivi Samsung', 'Hàng nhập khẩu', '', '8000000', '7500000', 1, 1, 10, 'm4.jpg'),
(6, 5, 'Tivi LG', '', '', '6000000', '5500000', 1, 1, 10, 'lg.jpg'),
(7, 2, 'Tủ lạnh Toshiba', '', '', '12000000', '9000000', 1, 1, 10, 'k2.jpg'),
(8, 2, 'Tủ lạnh Sony', '', '', '11000000', '10000000', 1, 1, 10, 'k2.jpg'),
(34, 10, 'Laptop Dell', 'Không có', 'Không có', '12000000', '11000000', 0, 0, 8, 'mk4.jpg'),
(37, 10, 'Laptop Acer', 'không', 'không', '10000000', '9500000', 0, 0, 8, 'mk5.jpg'),
(40, 1, 'Laptop Acer', 'khôngcó', 'không có', '9000000', '8000000', 0, 0, 8, 'mk5.jpg'),
(42, 1, 'Laptop Dell Inspiron', 'không', 'không', '9000000', '8000000', 0, 0, 5, 'vostro1.jpg'),
(44, 5, 'Tivi Sony', 'Smart tivi cơ bản43 . Ngang 97.3 cm - Cao 63 cm - Dày 27 cm', 'Chuyển động mượt ,Motionflow XR 200 HD R10HLG,Màu sắc sống động Live Colour,Nâng cấp hình ảnh X-Reality, PRO inch Full HD', '10000000', '8500000', 0, 0, 3, 'sony.jpg'),
(48, 4, 'Samsung s9', 'k', 'k', '4000000', '2500000', 0, 0, 3, 'mk9.jpg'),
(49, 3, 'Máy giặt Sony2', 'k', 'k', '9000000', '8000000', 0, 0, 8, 'Tsony.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `slider_id` int(11) NOT NULL,
  `slider_image` varchar(100) NOT NULL,
  `slider_caption` text NOT NULL,
  `slider_active` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_image`, `slider_caption`, `slider_active`) VALUES
(1, 'b4.jpg', 'Khuyến mãi lên đến 60%', 1),
(2, 'b4.jpg', 'slider 50%', 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Chỉ mục cho bảng `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Chỉ mục cho bảng `tbl_donhang`
--
ALTER TABLE `tbl_donhang`
  ADD PRIMARY KEY (`donhang_id`);

--
-- Chỉ mục cho bảng `tbl_giaodich`
--
ALTER TABLE `tbl_giaodich`
  ADD PRIMARY KEY (`giaodich_id`);

--
-- Chỉ mục cho bảng `tbl_giohang`
--
ALTER TABLE `tbl_giohang`
  ADD PRIMARY KEY (`giohang_id`);

--
-- Chỉ mục cho bảng `tbl_khachhang`
--
ALTER TABLE `tbl_khachhang`
  ADD PRIMARY KEY (`khachhang_id`);

--
-- Chỉ mục cho bảng `tbl_sanpham`
--
ALTER TABLE `tbl_sanpham`
  ADD PRIMARY KEY (`sanpham_id`);

--
-- Chỉ mục cho bảng `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `tbl_donhang`
--
ALTER TABLE `tbl_donhang`
  MODIFY `donhang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT cho bảng `tbl_giaodich`
--
ALTER TABLE `tbl_giaodich`
  MODIFY `giaodich_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT cho bảng `tbl_giohang`
--
ALTER TABLE `tbl_giohang`
  MODIFY `giohang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT cho bảng `tbl_khachhang`
--
ALTER TABLE `tbl_khachhang`
  MODIFY `khachhang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT cho bảng `tbl_sanpham`
--
ALTER TABLE `tbl_sanpham`
  MODIFY `sanpham_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT cho bảng `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
