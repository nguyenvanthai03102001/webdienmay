<?php
    include('../db/connect.php')
?>
<?php
   if(isset($_POST['themdanhmuc'])){
       $tendanhmuc=$_POST['danhmuc'];
       $sql_insert=mysqli_query($mysqli,"INSERT INTO tbl_category(category_name) values ('$tendanhmuc')");
   }
   if(isset($_GET['xoadanhmuc'])){
		$id= $_GET['xoadanhmuc'];
		$sql_xoa = mysqli_query($mysqli,"DELETE FROM tbl_category WHERE category_id='$id'");
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh mục</title>
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="collapse navbar-collapse" id="navbarNav" style="background-color: coral;">
	    <ul class="navbar-nav">
	      <li class="nav-item active">
	        <a class="nav-link" href="xulydonhang.php">Đơn hàng </a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="xulydanhmuc.php">Danh mục</a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="xulysanpham.php">Sản phẩm</a>
	      </li>
	       <li class="nav-item active">
         <a class="nav-link" href="xulykhachhang.php">Khách hàng</a>
	      </li>
	      
	    </ul>
	  </div>
	</nav>
    <div class="container" >
        <div class="row" >
        <div class="col-md-4" >
            <h4>Thêm danh mục</h4>
            <label> Tên danh mục</label>
            <form action="" method="POST">
            <input type="text" class="form-control" name="danhmuc" placeholder="Tên danh mục" >
            <input type="submit" name="themdanhmuc" value="Thêm danh mục" class="button" style="margin-top: 50px;">
            </form>
        </div>
        <div class="col-md-8" >
            <h4>Liệt kê danh mục</h4>
            <?php
            $sql_select=mysqli_query($mysqli,"SELECT * FROM tbl_category ORDER BY category_id DESC");//them sau len trc
            ?>
            <table class="table table-bordered " >
            <tr>
                <th>Thứ tự</th>
                <th>Tên danh mục</th>
                <th>Quản lí</th>
            </tr>
            <?php
            $i=0;
            while($row_category = mysqli_fetch_array($sql_select)){
                $i++;
            
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row_category['category_name']?></td>
                <td><a href="?xoadanhmuc=<?php echo $row_category['category_id'] ?>">Xóa</a></td>
            </tr>
            <?php
            }
            ?>
            </table>
        </div>
        </div>
    </div>
</body>
</html>