<?php
    include('../db/connect.php')
?>
<?php
   if(isset($_POST['themsanpham'])){
		$tensanpham = $_POST['tensanpham'];
		$hinhanh = $_FILES['hinhanh']['name'];
		
		$soluong = $_POST['soluong'];
		$gia = $_POST['giasanpham'];
		$giakhuyenmai = $_POST['giakhuyenmai'];
		$danhmuc = $_POST['danhmuc'];
		$chitiet = $_POST['chitiet'];
		$mota = $_POST['mota'];
		$path = '../uploads/';
		$hinhanh_tmp = $_FILES['hinhanh']['tmp_name'];
		$sql_insert_product = mysqli_query($mysqli,"INSERT INTO tbl_sanpham(sanpham_name,sanpham_chitiet,sanpham_mota,sanpham_gia,sanpham_giakhuyenmai,sanpham_soluong,sanpham_image,category_id) values ('$tensanpham','$chitiet','$mota','$gia','$giakhuyenmai','$soluong','$hinhanh','$danhmuc')");
		move_uploaded_file($hinhanh_tmp,$path.$hinhanh);
   }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sản phẩm</title>
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="collapse navbar-collapse" id="navbarNav" style="background-color: coral;">
	    <ul class="navbar-nav">
	      <li class="nav-item active">
	        <a class="nav-link" href="xulydonhang.php">Đơn hàng </a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="xulydanhmuc.php">Danh mục</a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="xulysanpham.php">Sản phẩm</a>
	      </li>
	       <li class="nav-item active">
         <a class="nav-link" href="xulykhachhang.php">Khách hàng</a>
	      </li>
	      
	    </ul>
	  </div>
	</nav>
  <br><br>
    <div class="container" style="background-color: aliceblue; border: 1px solid black; " >
        <div class="row" >
        <div class="col-md-4" style="border: 1px solid;">
            <h4 style="text-align: center;">Thêm sản phẩm</h4><br>
            <form action="" method="POST" enctype="multipart/form-data"> 
            <label> Tên sản phẩm</label>
            <input type="text" class="form-control" name="tensanpham" placeholder="Tên sản phẩm" ><br>
            <label> Hình ảnh</label>
            <input type="file" class="form-control" name="hinhanh" ><br>
            <label> Giá</label>
            <input type="text" class="form-control" name="giasanpham" placeholder="Giá sản phẩm" ><br>
            <label> Giá khuyến mãi</label>
            <input type="text" class="form-control" name="giakhuyenmai" placeholder="Giá khuyến mãi" ><br>
            <label> Số lượng</label>
            <input type="text" class="form-control" name="soluong" placeholder="Số lượng" ><br>
            <label>Mô tả</label>
					<textarea class="form-control" rows="10" name="mota"></textarea>
					<label>Chi tiết</label>
					<textarea class="form-control" rows="10" name="chitiet"></textarea><br>
            <label> Danh mục</label>
            <?php
            $sql_danhmuc = mysqli_query($mysqli,"SELECT * FROM tbl_category ORDER BY category_id DESC" );
            ?>
            <select name="danhmuc" class="form-control">
                <option value="0">----Chọn danh mục----</option>
                <?php
                while($row_danhmuc = mysqli_fetch_array($sql_danhmuc)){
                ?>
                <option value="<?php echo $row_danhmuc['category_id']?>"><?php echo $row_danhmuc['category_name']?></option>
                <?php 
                } 
                ?>
            </select><br>
            <input type="submit" name="themsanpham" value="Thêm sản phẩm" class="button" style="margin-top: 50px;"> <br>
            </form>
        </div>
        <div class="col-md-8" >
            <h4 style="text-align: center;">Liệt kê sản phẩm</h4><br>
            <?php
            $sql_select_sp=mysqli_query($mysqli,"SELECT * FROM tbl_sanpham, tbl_category WHERE tbl_sanpham.category_id=tbl_category.category_id ORDER BY tbl_sanpham.sanpham_id DESC");//them sau len trc
            ?>
            <table class="table table-bordered "  >
            <tr >
                <th >Thứ tự</th>
                <th>Tên sản phẩm</th>
                <th>Hình ảnh</th>
                <th>Số lượng</th>
                <th>Danh mục</th>
                <th>Giá sản phẩm</th>
                <th>Giá khuyến mại</th>
            </tr>
            <?php
            $i=0;
            while($row_sp = mysqli_fetch_array($sql_select_sp)){
                $i++;
            ?>
            <tr >
                <td><?php echo $i; ?></td>
                <td><?php echo $row_sp['sanpham_name'] ?> </td>
                <td><img src="../images/<?php echo $row_sp['sanpham_image'] ?>" height="120" width="150"></td>
                <td><?php echo $row_sp['sanpham_soluong'] ?></td>
                <td><?php echo $row_sp['category_name'] ?></td>
                <td><?php echo number_format($row_sp['sanpham_gia']).'vnd'  ?></td>
                <td><?php echo number_format($row_sp['sanpham_giakhuyenmai']).'vnd' ?></td>
            </tr>
            <?php
            }
            ?> 
            </table>
        </div>
        </div>
    </div>
</body>
</html>